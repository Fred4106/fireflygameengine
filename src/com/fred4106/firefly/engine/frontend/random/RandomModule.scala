package com.fred4106.firefly.engine.frontend.random

import com.fred4106.firefly.engine.frontend.core.Module

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
abstract class RandomModule extends Module {
	def getRandomInt(): Int

	def getRandomInt(cap: Int): Int

	def getRandomInt(min: Int, max: Int): Int

	def getRandomFloat(): Float

	def getUUID(): String
}