package com.fred4106.firefly.engine.frontend

import com.fred4106.firefly.engine.frontend.core.LoggableModule

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 4/8/2015
 * for FireflyGameEngine
 */
object FireflyEventDistributor extends LoggableModule {
	val matrix: ArrayBuffer[FireflyEvent] = new ArrayBuffer[FireflyEvent]()
	val callbacks: ArrayBuffer[((FireflyEvent) => Boolean, (FireflyEvent) => Unit)] = ArrayBuffer[((FireflyEvent) => Boolean, (FireflyEvent) => Unit)]()

	def registerCallback(trigger: (FireflyEvent) => Boolean, callback: (FireflyEvent) => Unit): Unit = {
		callbacks += ((trigger, callback))
	}

	def pushEvent(event: FireflyEvent): Unit = {
		matrix += event
	}

	def poke(): Unit = {
		if (matrix.length > 0) {
			val event = matrix.head
			matrix.remove(0)
			callbacks.foreach((callback: ((FireflyEvent) => Boolean, (FireflyEvent) => Unit)) => {
				if (callback._1(event)) {
					callback._2(event)
				}
			})
		}
	}

	override def getModuleName(): String = "FireflyEventDistributor"
}

class FireflyEvent(val message: String) {
	def getMessage(): String = message
}