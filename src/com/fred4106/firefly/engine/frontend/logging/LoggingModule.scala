package com.fred4106.firefly.engine.frontend.logging

import com.fred4106.firefly.engine.frontend.core.Module

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
abstract class LoggingModule extends Module {
	def debug(name: String, message: String): Unit

	def info(name: String, message: String): Unit

	def message(name: String, message: String): Unit

	def config(name: String, message: String): Unit

	def warning(name: String, message: String): Unit

	def severe(name: String, message: String): Unit

	def error(name: String, message: String): Unit

	def exception(name: String, exception: Exception): Unit
}