package com.fred4106.firefly.engine.frontend

import com.fred4106.firefly.engine.frontend.core.Frontend
import com.fred4106.firefly.engine.frontend.logging.LoggingModule

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
object FireflyLogging extends LoggingModule with Frontend[LoggingModule] {
	private val nulled: LoggingModule = new LoggingModule {
		override def severe(name: String, message: String): Unit = {}

		override def config(name: String, message: String): Unit = {}

		override def warning(name: String, message: String): Unit = {}

		override def error(name: String, message: String): Unit = {}

		override def debug(name: String, message: String): Unit = {}

		override def message(name: String, message: String): Unit = {}

		override def info(name: String, message: String): Unit = {}

		override def terminate(): Unit = {}

		override def initalize(): Unit = {}

		override def exception(name: String, exception: Exception): Unit = {}
	}

	private var wrapped: LoggingModule = nulled

	override def shutdown(): Unit = {
		provide(nulled)
	}

	override def provide(wrap: LoggingModule): Unit = {
		super.provide(wrap)
		wrapped.terminate()
		wrapped = wrap
		wrapped.initalize()
	}

	override def debug(name: String, message: String): Unit = wrapped.debug(name, message)

	override def severe(name: String, message: String): Unit = wrapped.severe(name, message)

	override def config(name: String, message: String): Unit = wrapped.config(name, message)

	override def warning(name: String, message: String): Unit = wrapped.warning(name, message)

	override def error(name: String, message: String): Unit = wrapped.error(name, message)

	override def message(name: String, message: String): Unit = wrapped.message(name, message)

	override def info(name: String, message: String): Unit = wrapped.info(name, message)

	override def terminate(): Unit = {}

	override def initalize(): Unit = {}

	override def exception(name: String, exception: Exception): Unit = wrapped.exception(name, exception)

	override def getModuleName(): String = "FireflyLogging"
}