package com.fred4106.firefly.engine.frontend.executor

import com.fred4106.firefly.engine.frontend.core.Module

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
abstract class ExecutorModule extends Module {
	def startTask(t: Task, delay: Int): String

	def startClockTask(t: Task, clock: Int, delay: Int, lifetime: Int): String

	def killTask(uuid: String): Unit
}