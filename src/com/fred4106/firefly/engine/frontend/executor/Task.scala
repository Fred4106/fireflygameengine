package com.fred4106.firefly.engine.frontend.executor

import com.fred4106.firefly.engine.frontend.core.UUIDable
import com.fred4106.firefly.engine.frontend.{FireflyExecutor, FireflyLogging}

/**
 * Created by Nathaniel on 2/8/2015
 * for FireflyGameEngine
 */
abstract class Task() extends Runnable with UUIDable {

	override def run(): Unit = {
		try {
			FireflyExecutor.log(s"Running task [${getUUID()}]")
			this.onRun()
		} catch {
			case e: Exception => FireflyLogging.exception(getUUID(), e)
		}
	}

	def onRun(): Unit
}

object TaskFactory {
	def generateTask(f: () => Unit): Task = {
		new Task {
			override def onRun(): Unit = f()
		}
	}
}