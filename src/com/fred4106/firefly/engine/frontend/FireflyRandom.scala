package com.fred4106.firefly.engine.frontend

import com.fred4106.firefly.engine.frontend.core.Frontend
import com.fred4106.firefly.engine.frontend.random.RandomModule

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
object FireflyRandom extends RandomModule with Frontend[RandomModule] {
	private val nulled: RandomModule = new RandomModule {
		override def getUUID(): String = "null"

		override def getRandomInt(): Int = 0

		override def getRandomInt(cap: Int): Int = 0

		override def getRandomInt(min: Int, max: Int): Int = 0

		override def getRandomFloat(): Float = 0f

		override def terminate(): Unit = {}

		override def initalize(): Unit = {}
	}
	private var wrapped: RandomModule = nulled

	override def getRandomInt(): Int = {
		log(s"Generating random int")
		val temp = wrapped.getRandomInt()
		log(s"Generated random int [$temp]")
		temp
	}

	override def getUUID(): String = {
		log(s"Generating UUID")
		val temp = wrapped.getUUID()
		log(s"Generated UUID [$temp]")
		temp
	}

	override def getRandomInt(cap: Int): Int = {
		log(s"Generating random int with cap [$cap]")
		val temp = wrapped.getRandomInt(cap)
		log(s"Generated random int [$temp]")
		temp
	}

	override def getRandomInt(min: Int, max: Int): Int = {
		log(s"Generating random int with min [$min] and max [$max]")
		val temp = wrapped.getRandomInt(min, max)
		log(s"Generated random int [$temp]")
		temp
	}

	override def getRandomFloat(): Float = {
		log(s"Generating random float")
		val temp = wrapped.getRandomFloat()
		log(s"Generated random float [$temp]")
		temp
	}

	override def shutdown(): Unit = {
		super.shutdown()
		provide(nulled)
	}

	override def provide(wrap: RandomModule): Unit = {
		super.provide(wrap)
		wrapped.terminate()
		wrapped = wrap
		wrapped.initalize()
	}

	override def terminate(): Unit = {}

	override def initalize(): Unit = {}

	override def getModuleName(): String = "FireflyRandom"
}