package com.fred4106.firefly.engine.frontend.core

import com.fred4106.firefly.engine.frontend.FireflyLogging

/**
 * Created by Nathaniel on 4/9/2015
 * for FireflyGameEngine
 */
trait LoggableModule {
	private var logging: Boolean = false

	def enableLogging(): Unit = logging = true

	def disableLogging(): Unit = logging = false

	def setLogging(bool: Boolean): Unit = logging = bool

	def getModuleName(): String

	def log(message: String): Unit = {
		if (logging) {
			FireflyLogging.debug(getModuleName(), message)
		}
	}

}