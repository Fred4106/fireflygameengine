package com.fred4106.firefly.engine.frontend.core

/**
 * Created by Nathaniel on 2/7/2015
 * for FireflyGameEngine
 */
trait Module {
	def terminate(): Unit

	def initalize(): Unit
}