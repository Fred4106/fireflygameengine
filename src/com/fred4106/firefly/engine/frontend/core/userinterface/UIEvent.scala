package com.fred4106.firefly.engine.frontend.core.userinterface

import com.fred4106.firefly.engine.frontend.core.UUIDable

/**
 * Created by Nathaniel on 4/15/2015
 * for FireflyGameEngine
 */
class UIEvent extends UUIDable {}

class UIMouseEvent extends UIEvent {}

class UIMouseClickedEvent(val pos: (Int, Int), val btn: Int) extends UIMouseEvent {}

class UIMousePressedEvent(val pos: (Int, Int), val btn: Int) extends UIMouseEvent {}

class UIMouseReleasedEvent(val pos: (Int, Int), val btn: Int) extends UIMouseEvent {}

class UIMouseMovedEvent(val pos1: (Int, Int), val pos2: (Int, Int)) extends UIMouseEvent {}

class UIMouseDraggedEvent(pos1: (Int, Int), pos2: (Int, Int), val btn: Int) extends UIMouseMovedEvent(pos1, pos2) {}