package com.fred4106.firefly.engine.frontend.core.userinterface

import com.fred4106.firefly.engine.frontend.core.{FireflyGraphics, UUIDable}

/**
 * Created by Nathaniel on 5/13/2015
 * for FireflyGameEngine
 */
abstract class InterfaceElement extends UUIDable {
	def draw(size: (Int, Int)): FireflyGraphics

	def handleEvent(uIEvent: UIEvent): Unit
}