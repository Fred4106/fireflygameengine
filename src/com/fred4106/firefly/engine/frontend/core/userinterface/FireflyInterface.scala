package com.fred4106.firefly.engine.frontend.core.userinterface

import com.fred4106.firefly.engine.frontend.core.UUIDable

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 5/13/2015
 * for FireflyGameEngine
 */
class FireflyInterface extends UUIDable {
	private val elements: ArrayBuffer[(InterfaceElement, (Float, Float), (Float, Float))] = new ArrayBuffer[(InterfaceElement, (Float, Float), (Float, Float))]()

	def addElement(ele: InterfaceElement, pos: (Float, Float), size: (Float, Float)): String = {
		elements += ((ele, pos, size))
		ele.getUUID()
	}

	def getElement(uuid: String): InterfaceElement = {
		getElementData(uuid)._1
	}

	def getElementData(uuid: String): (InterfaceElement, (Float, Float), (Float, Float)) = {
		var toReturn: (InterfaceElement, (Float, Float), (Float, Float)) = null
		elements.foreach(
			(f: (InterfaceElement, (Float, Float), (Float, Float))) => {
				if (f._1.getUUID().equals(uuid)) {
					toReturn = f
				}
			}
		)
		toReturn
	}

	def getKeySet(): ArrayBuffer[String] = {
		val buffer = new ArrayBuffer[String]()
		elements.foreach(
			(f: (InterfaceElement, (Float, Float), (Float, Float))) => {
				buffer += f._1.getUUID()
			}
		)
		buffer
	}

	def setElementPosition(uuid: String, pos: (Float, Float)): Unit = {
		val temp = getElementData(uuid)
		elements.remove(elements.indexOf(temp))
		elements += ((temp._1, pos, temp._3))
	}

	def setElementSize(uuid: String, size: (Float, Float)): Unit = {
		val temp = getElementData(uuid)
		elements.remove(elements.indexOf(temp))
		elements += ((temp._1, temp._2, size))
	}

	def setElementData(uuid: String, pos: (Float, Float), size: (Float, Float)): Unit = {
		val temp = getElementData(uuid)
		elements.remove(elements.indexOf(temp))
		elements += ((temp._1, pos, size))
	}
}