package com.fred4106.firefly.engine.frontend.core.userinterface

import com.fred4106.firefly.engine.frontend.core.FireflyGraphics

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 5/13/2015
 * for FireflyGameEngine
 */
class InterfaceEngine {
	private val elements: ArrayBuffer[FireflyInterface] = new ArrayBuffer[FireflyInterface]()

	private var activeUUID: String = "null"

	def addInterface(ele: FireflyInterface): String = {
		elements += ele
		if (activeUUID == "null") {
			setActiveInterface(ele.getUUID())
		}
		ele.getUUID()
	}

	def setActiveInterface(uuid: String): Unit = {
		activeUUID = uuid
	}

	def drawInterface(size: (Int, Int)): FireflyGraphics = {
		val graphic = new FireflyGraphics(size)
		graphic.background(0, 0)

		val activeInterface = getInterface(activeUUID)

		val uuids = activeInterface.getKeySet()

		uuids.foreach(
			(uuid: String) => {
				val data = activeInterface.getElementData(uuid)
				val width: Int = (data._3._1 * size._1).asInstanceOf[Int]
				val height: Int = (data._3._2 * size._2).asInstanceOf[Int]
				val eleGraphic = data._1.draw((width, height))

				graphic.image(eleGraphic, data._2._1 * size._1, data._2._2 * size._2)
			}
		)

		graphic
	}

	def getInterface(uuid: String): FireflyInterface = {
		var ele: FireflyInterface = null
		elements.foreach(
			(u: FireflyInterface) => {
				if (u.getUUID() == uuid) {
					ele = u
				}
			}
		)
		ele
	}
}
