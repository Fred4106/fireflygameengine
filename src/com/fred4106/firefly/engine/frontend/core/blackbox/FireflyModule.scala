package com.fred4106.firefly.engine.frontend.core.blackbox

import com.fred4106.firefly.engine.frontend.core.{FireflyGraphics, UUIDable}

/**
 * Created by Nathaniel on 5/5/2015
 * for FireflyGameEngine
 */
abstract class FireflyModule extends UUIDable {
	def render(size: (Int, Int)): FireflyGraphics

	def tick(): Unit

	def passEvent(): Unit = {
		//TODO work on events
	}
}

class FireflyOverworldModule(fireflyWorld: FireflyWorld, fireflyCamera: FireflyCamera) extends FireflyModule {
	def getCamera(): FireflyCamera = {
		fireflyCamera
	}

	override def render(size: (Int, Int)): FireflyGraphics = {
		getWorld().render(size, fireflyCamera)
	}

	def getWorld(): FireflyWorld = {
		fireflyWorld
	}

	override def tick(): Unit = {
		getWorld().tick()
	}
}