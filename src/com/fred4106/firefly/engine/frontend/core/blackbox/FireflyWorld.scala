package com.fred4106.firefly.engine.frontend.core.blackbox

import com.fred4106.firefly.engine.frontend.core.{FireflyGraphics, UUIDable}

/**
 * Created by Nathaniel on 4/23/2015
 * for FireflyGameEngine
 */
abstract class FireflyWorld extends UUIDable {
	def tick(): Unit

	def render(size: (Int, Int), camera: FireflyCamera): FireflyGraphics
}