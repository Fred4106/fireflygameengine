package com.fred4106.firefly.engine.frontend.core.blackbox

/**
 * Created by Nathaniel on 4/23/2015
 * for FireflyGameEngine
 */
class FireflyCamera(var pos: (Float, Float)) {
	def translate(offset: (Float, Float)): Unit = {
		pos = (pos._1 + offset._1, pos._2 + offset._2)
	}

	def setPos(pos: (Float, Float)): Unit = {
		this.pos = pos
	}
}