package com.fred4106.firefly.engine.frontend.core.blackbox

import com.fred4106.firefly.engine.frontend.FireflyExecutor
import com.fred4106.firefly.engine.frontend.core.FireflyGraphics
import com.fred4106.firefly.engine.frontend.core.userinterface.UIEvent
import com.fred4106.firefly.engine.frontend.executor.TaskFactory

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
//Needs to take events in.
//Tics the world
//outputs a frame
class FireflyBlackbox() {

	private var ticRate: Int = 4
	//this is how many times the box tics per second
	private var ticUuid: String = "" //stores the uuid of the task that tics the blackbox.

	private var modules: ArrayBuffer[FireflyModule] = new ArrayBuffer[FireflyModule]
	private var activeUUID: String = "null"

	def initialize(): Unit = {
		ticUuid = FireflyExecutor.startClockTask(TaskFactory.generateTask(tick), 1000 / ticRate, 0, 0)
	}

	def addModule(fireflyModule: FireflyModule): String = {
		modules += fireflyModule
		if (activeUUID == "null") {
			setActiveModule(fireflyModule.getUUID())
		}

		fireflyModule.getUUID()
	}

	def setActiveModule(uuid: String): Boolean = {
		if (getModule(uuid) != null) {
			activeUUID = uuid
			true
		} else {
			false
		}
	}

	def getModule(uuid: String): FireflyModule = {
		var module: FireflyModule = null
		modules.foreach(
			(f: FireflyModule) => {
				if (f.getUUID() == uuid)
					module = f
			}
		)
		module
	}

	def changeTicRate(ticRate: Int): Unit = {
		FireflyExecutor.killTask(ticUuid)
		this.ticRate = ticRate
		ticUuid = FireflyExecutor.startClockTask(TaskFactory.generateTask(tick), 1000 / ticRate, 0, 0)
	}

	private def tick(): Unit = {
		getModule(activeUUID).tick()
	}

	def passEvent(event: UIEvent): Unit = {
		//something to do with size?
	}

	def getFrame(size: (Int, Int)): FireflyGraphics = {
		//TODO come up with a way to do interface overlay.
		getModule(activeUUID).render(size)
	}
}