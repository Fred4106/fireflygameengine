package com.fred4106.firefly.engine.frontend.core

import com.fred4106.firefly.engine.frontend.FireflyRandom

/**
 * Created by Nathaniel on 3/29/2015
 * for FireflyGameEngine
 */
trait UUIDable {
	private var uuid: String = FireflyRandom.getUUID()

	def getUUID(): String = this.uuid

	def setUUID(uuid: String): Unit = {
		this.uuid = uuid
	}
}