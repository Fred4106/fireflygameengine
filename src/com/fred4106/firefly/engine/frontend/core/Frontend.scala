package com.fred4106.firefly.engine.frontend.core

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
trait Frontend[E <: Module] extends LoggableModule {
	def provide(wrap: E): Unit = {
		log(s"Providing new backend [${wrap.getClass.getCanonicalName}]")
	}

	def shutdown(): Unit = {
		log(s"Shutting down")
	}
}