package com.fred4106.firefly.engine.frontend.core

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Nathaniel on 2/21/2015
 * for FireflyGameEngine
 */
class FireflyGraphics(width: Int, height: Int) {
	private val size = (width, height)
	private val queue: ArrayBuffer[DrawCall] = new ArrayBuffer[DrawCall]

	def this(size: (Int, Int)) = this(size._1, size._2)

	def getSize(): (Int, Int) = size

	def pushMatrix(): Unit = queue += new PushDrawCall

	def popMatrix(): Unit = queue += new PopDrawCall

	def translate(xDist: Float, yDist: Float): Unit = queue += new TranslateDrawCall(xDist, yDist)

	def rotate(degrees: Float): Unit = queue += new RotateDrawCall(degrees)

	def background(grey: Int): Unit = {
		this.background(grey, grey, grey)
	}

	def background(red: Int, green: Int, blue: Int): Unit = {
		background(red, green, blue, 255)
	}

	def background(red: Int, green: Int, blue: Int, opacity: Int): Unit = {
		queue += new BackgroundDrawCall(red, green, blue, opacity)
	}

	def background(grey: Int, opacity: Int): Unit = {
		background(grey, grey, grey, opacity)
	}

	def fill(red: Int, green: Int, blue: Int): Unit = {
		fill(red, green, blue, 255)
	}

	def fill(red: Int, green: Int, blue: Int, opacity: Int): Unit = queue += new FillDrawCall(red, green, blue, opacity)

	def fill(grey: Int): Unit = {
		fill(grey, 0)
	}

	def fill(grey: Int, opacity: Int): Unit = {
		fill(grey, grey, grey, opacity)
	}

	def stroke(red: Int, green: Int, blue: Int): Unit = {
		stroke(red, green, blue, 255)
	}

	def stroke(red: Int, green: Int, blue: Int, opacity: Int): Unit = queue += new StrokeDrawCall(red, green, blue, opacity)

	def stroke(grey: Int): Unit = {
		stroke(grey, 0)
	}

	def stroke(grey: Int, opacity: Int): Unit = {
		stroke(grey, grey, grey, opacity)
	}

	def image(inImage: FireflyGraphics, xPos: Float, yPos: Float): Unit = {
		image(inImage, xPos, yPos, inImage.getXSize(), inImage.getYSize())
	}

	def getXSize(): Int = size._1

	def getYSize(): Int = size._2

	def image(inImage: FireflyGraphics, xPos: Float, yPos: Float, width: Float, height: Int): Unit = {
		queue += new ImageDrawCall(inImage, xPos, yPos, width, height)
	}

	def ellipse(xPos: Float, yPos: Float, diameter: Float): Unit = {
		ellipse(xPos, yPos, diameter, diameter)
	}

	def ellipse(xPos: Float, yPos: Float, width: Float, height: Float): Unit = {
		queue += new EllipseDrawCall(xPos, yPos, width, height)
	}

	def rect(xPos: Float, yPos: Float, dimension: Float): Unit = {
		rect(xPos, yPos, dimension, dimension)
	}

	def rect(xPos: Float, yPos: Float, width: Float, height: Float): Unit = queue += new RectDrawCall(xPos, yPos, width, height)

	def line(p1: (Float, Float), p2: (Float, Float)): Unit = {
		line(p1._1, p1._2, p2._1, p2._2)
	}

	def line(p1x: Float, p1y: Float, p2x: Float, p2y: Float): Unit = queue += new LineDrawCall(p1x, p1y, p2x, p2y)

	def getDrawStack(): Array[DrawCall] = queue.toArray

	//TODO create a queue of First in First out to use to store draw calls

}

class DrawCall {}

class LineDrawCall(val p1x: Float, val p1y: Float, val p2x: Float, val p2y: Float) extends DrawCall {}

class RectDrawCall(val xPos: Float, val yPos: Float, val width: Float, val height: Float) extends DrawCall {}

class EllipseDrawCall(val xPos: Float, val yPos: Float, val width: Float, val height: Float) extends DrawCall {}

class ImageDrawCall(val inImage: FireflyGraphics, val xPos: Float, val yPos: Float, val width: Float, val height: Float)
		extends DrawCall {}

class StrokeDrawCall(val red: Int, val green: Int, val blue: Int, val opacity: Int) extends DrawCall {}

class FillDrawCall(val red: Int, val green: Int, val blue: Int, val opacity: Int) extends DrawCall {}

class BackgroundDrawCall(val red: Int, val green: Int, val blue: Int, val opacity: Int) extends DrawCall {}

class RotateDrawCall(val degrees: Float) extends DrawCall {}

class TranslateDrawCall(val xDist: Float, val yDist: Float) extends DrawCall {}

class PushDrawCall extends DrawCall {}

class PopDrawCall extends DrawCall {}