package com.fred4106.firefly.engine.frontend

import com.fred4106.firefly.engine.frontend.core.Frontend
import com.fred4106.firefly.engine.frontend.executor.{ExecutorModule, Task}

/**
 * Created by Nathaniel on 4/7/2015
 * for FireflyGameEngine
 */
object FireflyExecutor extends ExecutorModule with Frontend[ExecutorModule] {
	private val nulled: ExecutorModule = new ExecutorModule {
		override def killTask(uuid: String): Unit = {}

		override def startTask(t: Task, delay: Int): String = "nulled"

		override def startClockTask(t: Task, clock: Int, delay: Int, lifetime: Int): String = "nulled"

		override def terminate(): Unit = {}

		override def initalize(): Unit = {}
	}
	private var wrapped: ExecutorModule = nulled

	override def startTask(t: Task, delay: Int): String = {
		log(s"Starting task [${t.getUUID()}] with delay[$delay]")
		wrapped.startTask(t, delay)
	}

	override def killTask(uuid: String): Unit = {
		log(s"Killing task with uuid [$uuid]")
		wrapped.killTask(uuid)
	}

	override def startClockTask(t: Task, clock: Int, delay: Int, lifetime: Int): String = {
		log(s"Starting clock task with uuid [${t.getUUID()}] and clock [$clock] and delay [$delay] and lifetime[$lifetime]")
		wrapped.startClockTask(t, clock, delay, lifetime)
	}

	override def shutdown(): Unit = super.shutdown();
	provide(nulled)

	override def provide(wrap: ExecutorModule): Unit = {
		super.provide(wrap)
		wrapped.terminate()
		wrapped = wrap
		wrap.initalize()
	}

	override def terminate(): Unit = {}

	override def initalize(): Unit = {}

	override def getModuleName(): String = "FireflyExecutor"
}