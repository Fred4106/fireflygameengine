package com.fred4106.firefly.engine.resource

import java.io.File

import org.apache.commons.io.FileUtils

/**
 * Created by Nathaniel on 1/25/2015
 * for FireflyGameEngine
 */
@SuppressWarnings(Array("ALL"))
object JSONLoader {
	def loadJSON(fileName: String): JSONObject = {
		val file = new File(fileName)
		val string = FileUtils.readFileToString(file)
		new JSONObject(new JSONTokener(string))
	}
}