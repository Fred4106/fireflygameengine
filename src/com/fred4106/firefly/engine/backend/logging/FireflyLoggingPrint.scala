package com.fred4106.firefly.engine.backend.logging

import com.fred4106.firefly.engine.frontend.logging.LoggingModule
import org.apache.commons.lang3.exception.ExceptionUtils

/**
 * Created by Nathaniel on 2/20/2015
 * for FireflyGameEngine
 */
class FireflyLoggingPrint extends LoggingModule {

	override def debug(name: String, message: String): Unit = handleMessage(s"[Debug] | [$name] | [$message]")

	override def severe(name: String, message: String): Unit = handleMessage(s"[Severe] | [$name] | [$message]")

	override def config(name: String, message: String): Unit = handleMessage(s"[Config] | [$name] | [$message]")

	override def warning(name: String, message: String): Unit = handleMessage(s"[Warning] | [$name] | [$message]")

	override def error(name: String, message: String): Unit = handleMessage(s"[Error] | [$name] | [$message]")

	protected def handleMessage(message: String): Unit = {
		println(message)
	}

	override def message(name: String, message: String): Unit = handleMessage(s"[Message] | [$name] | [$message]")

	override def info(name: String, message: String): Unit = handleMessage(s"[Info] | [$name] | [$message]")

	override def exception(name: String, exception: Exception): Unit = handleMessage(s"[Exception] | [$name]\n${
		ExceptionUtils.getStackTrace(exception)
	}")

	override def terminate(): Unit = {}

	override def initalize(): Unit = {}
}