package com.fred4106.firefly.engine.backend.logging

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

/**
 * Created by Nathaniel on 2/8/2015
 * for FireflyGameEngine
 */
class FireflyLoggingFile(fileName: String) extends FireflyLoggingPrint {

	var outFile: File = null
	var outStream: PrintWriter = null

	// new PrintWriter(new BufferedWriter(new FileWriter(outFile, true)))
	override def handleMessage(message: String): Unit = {
		outStream.println(message)
	}

	override def terminate(): Unit = {
		outStream.flush()
		outStream.close()
	}

	override def initalize(): Unit = {
		outFile = new File(fileName)
		outFile.delete()
		outFile.createNewFile()
		outStream = new PrintWriter(new BufferedWriter(new FileWriter(outFile, true)))
	}

}