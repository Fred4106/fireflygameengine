package com.fred4106.firefly.engine.backend.executor

import java.util.concurrent._

import com.fred4106.firefly.engine.frontend.FireflyExecutor
import com.fred4106.firefly.engine.frontend.executor.{ExecutorModule, Task}

import scala.collection.mutable.HashMap

/**
 * Created by Nathaniel on 2/8/2015
 * for FireflyGameEngine
 */
class FireflyExecutorReal extends ExecutorModule {
	var executor: ScheduledExecutorService = null
	var tasks: HashMap[String, ScheduledFuture[_]] = null

	override def killTask(uuid: String): Unit = {
		if (tasks.contains(uuid)) {
			tasks.get(uuid).get.cancel(true)
		}
	}

	override def terminate(): Unit = {
		executor.shutdownNow()
	}

	override def startTask(t: Task, delay: Int): String = {
		tasks.put(t.getUUID(), executor.schedule(t, delay, TimeUnit.MILLISECONDS))
		t.getUUID()
	}

	override def startClockTask(t: Task, clock: Int, delay: Int, lifetime: Int): String = {
		tasks.put(t.getUUID(), executor.scheduleAtFixedRate(t, delay, clock, TimeUnit.MILLISECONDS))
		if (lifetime > 0) {
			killTaskWithDelay(t.getUUID(), lifetime - 100)
		}
		t.getUUID()
	}

	private def killTaskWithDelay(uuid: String, delay: Int): Unit = {
		FireflyExecutor.startTask(new KillTask(uuid), delay)
	}

	override def initalize(): Unit = {
		executor = Executors.newSingleThreadScheduledExecutor()
		tasks = new HashMap[String, ScheduledFuture[_]]()
	}

	private class KillTask(uuid: String) extends Task {
		override def onRun(): Unit = FireflyExecutor.killTask(uuid)
	}

}