package com.fred4106.firefly.engine.backend.random

import java.util.{Random, UUID}

import com.fred4106.firefly.engine.frontend.random.RandomModule

/**
 * Created by Nathaniel on 3/29/2015
 * for FireflyGameEngine
 */
class FireflyRandomReal extends RandomModule {
	private var random: Random = null

	override def getRandomInt(): Int = random.nextInt()

	override def getUUID(): String = UUID.randomUUID().toString()

	override def getRandomInt(min: Int, max: Int): Int = {
		getRandomInt(max - min) + min
	}

	override def getRandomInt(cap: Int): Int = {
		random.nextInt(cap)
	}

	override def getRandomFloat(): Float = random.nextFloat()

	override def terminate(): Unit = {}

	override def initalize(): Unit = {
		random = new Random()
	}
}